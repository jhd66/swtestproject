const axios = require("axios");
const {prepare} = require("../setup/test-helper");

describe("OrderBot API Tests - Users", () => {

  let adminLogin = null;
  let genericConfig = null;

  beforeAll(async () => {
    // Login admin user.

    adminLogin = await axios.post(prepare("/login"), {
      email: "test@test.com",
      password: "12345"
    });

    const {accessToken} = adminLogin.data;

    // Note we use JWT authentication for the API,
    // therefore we need to authenticate our request for the test.
    genericConfig = {
      headers: { Authorization: `Bearer ${accessToken}` }
    } 
  });

    // User registration tests
    it("should register user successfully", async () => {
      const userRegister = await axios.post(prepare("/register"), {
        name: "Succesful",
        role: "User",
        email: "succesfuluser@test.com",
        password: "12345",
        address: "Somewhere 10"
      });
    
      const {status, data} = userRegister;
      expect(status).toEqual(201);
      expect(data).toHaveProperty("email", "succesfuluser@test.com");
    });

    it("should fail to register user with duplicate email", async () => {
      await axios.post(prepare("/register"), {
        name: "User",
        role: "User",
        email: "testuser@test.com",
        password: "12345",
        address: "Somewhere 10"
      }).catch((error) => {
        const {response} = error;
        expect(response.status).toEqual(409);
        expect(response.data.message).toEqual("User Exists.");
      });
    });

    it("should fail to register user with invalid email format", async () => {
      await axios.post(prepare("/register"), {
        name: "User",
        role: "User",
        email: "invalidemailformat",
        password: "12345",
        address: "Somewhere 10"
      }).catch((error) => {
        const {response} = error;
        expect(response.status).toEqual(400);
        expect(response.data.message).toEqual("Email is in bad form.");
      });
    });

    it("should fail to register user (no password)", async () => {
      await axios.post(prepare("/register"), {
        "name": "User New",
        "role": "User",
        "email": "testuseranothernew2@test.com",
        "address": "Somewhere 10"
      }).catch(error => {
        expect(error.response.status).toEqual(400);
      });
    });
  
    it("should fail to register user (no name)", async () => {
      await axios.post(prepare("/register"), {
        "role": "User",
        "email": "testuseranothernew3@test.com",
        "password": "12345",
        "address": "Somewhere 10"
      }).catch(error => {
        expect(error.response.status).toEqual(400);
      });
    });
  
    it("should fail to register user (no address)", async () => {
      await axios.post(prepare("/register"), {
        "name": "User New",
        "role": "User",
        "email": "testuseranothernew4@test.com",
        "password": "12345",
      }).catch(error => {
        expect(error.response.status).toEqual(400);
      });
    });

    it("should fail to register user with missing required fields", async () => {
      await axios.post(prepare("/register"), {
        name: "User",
        role: "User"
        // Missing email, password, and address
      }).catch((error) => {
        const {response} = error;
        expect(response.status).toEqual(400);
        expect(response.data.message).toContain("Email is in bad form.");
      });
    });

    // User login tests
    it("should login user", async () => {
      const userLogin = await axios.post(prepare("/login"), {
        email: "testuser@test.com",
        password: "12345"
    });

      const {accessToken} = userLogin.data;
      expect(userLogin.status).toEqual(200);
      expect(accessToken).not.toEqual(undefined);
  });

    it("should fail to login user (invalid email)", async () => {
      await axios.post(prepare("/login"), {
        email: "testuser@test.co",
        password: "1234567"
      }).catch((error) => {
        const {response} = error;
        const {accessToken} = response.data;
        expect(response.status).toEqual(404);
        expect(accessToken).toEqual(undefined);
      });
      
    });

    it("should fail to login user (wrong password)", async () => {
      await axios.post(prepare("/login"), {
        email: "testuser@test.com",
        password: "1234567"
      }).catch((error) => {
        const {response} = error;
        const {accessToken} = response.data;
        expect(response.status).toEqual(401);
        expect(accessToken).toEqual(null);
      });
      
    });

    it("should fail to login user (no email or password)", async () => {
      await axios.post(prepare("/login"), {
        email: "",
        password: ""
      }).catch((error) => {
        const {response} = error;
        const {accessToken} = response.data;
        expect(response.status).toEqual(404);
        expect(accessToken).toEqual(undefined);
      });
      
    });

    // User get info tests
    it("should retrieve user info with valid user ID", async () => {
      // Assuming you have a function to login and get a token
      const loginResponse = await axios.post(prepare("/login"), {
        email: "testuser@test.com",
        password: "12345"
      });
    
      const {accessToken} = loginResponse.data;
    
      const userInfo = await axios.get(prepare("/me"), {
        headers: { Authorization: `Bearer ${accessToken}` }
      });
    
      expect(userInfo.status).toEqual(200);
      expect(userInfo.data).toHaveProperty("email", "testuser@test.com");
    });

    it("should fail to retrieve user info with invalid token", async () => {
      await axios.get(prepare("/me"), {
        headers: { Authorization: `Bearer invalid_or_expired_token` }
      }).catch((error) => {
        const {response} = error;
        expect(response.status).toEqual(400);
        expect(response.data.message).toEqual("Unauthorized access.");
      });
    });

    // User update tests
    it("should update user details with valid data", async () => {
      // Login and get a token
      const loginResponse = await axios.post(prepare("/login"), {
        email: "testuser@test.com",
        password: "12345"
      });
      const {accessToken} = loginResponse.data;
    
      // Update user details
      const updatedUserInfo = await axios.put(prepare("/me"), {
        name: "Updated Name",
        address: "Updated Address"
      }, {
        headers: { Authorization: `Bearer ${accessToken}` }
      });
    
      expect(updatedUserInfo.status).toEqual(201);
      expect(updatedUserInfo.data).toHaveProperty("name", "Updated Name");
      expect(updatedUserInfo.data).toHaveProperty("address", "Updated Address");
    });

    it("should return 'No user found' when updating non-existent user", async () => {
      // Login and get a token
      const loginResponse = await axios.post(prepare("/login"), {
        email: "testuser@test.com",
        password: "12345"
      });
      const {accessToken} = loginResponse.data + '1';
    
      // Attempt to update user details
      await axios.put(prepare("/me"), {
        name: "New Name",
        address: "New Address"
      }, {
        headers: { Authorization: `Bearer ${accessToken}` }
      }).catch((error) => {
        const {response} = error;
        expect(response.status).toEqual(400);
        expect(response.data.message).toEqual("No user found.");
      });
    });

    it("should fail to update user details with invalid data format", async () => {
      // Login and get a token
      const loginResponse = await axios.post(prepare("/login"), {
        email: "testuser@test.com",
        password: "12345"
      });
      const {accessToken} = loginResponse.data;
    
      // Attempt to update user details with invalid format
      await axios.put(prepare("/me"), {
        name: 12345, // Invalid format: name should be a string
        address: "Valid Address"
      }, {
        headers: { Authorization: `Bearer ${accessToken}` }
      }).catch((error) => {
        const {response} = error;
        expect(response.status).toEqual(400);
        expect(response.data.message).toContain("Bad Request.");
      });
    });

    // User deletion tests
    it("should delete a user with a valid ID by an admin", async () => {
      const adminLoginResponse = await axios.post(prepare("/login"), {
        email: "test@test.com",
        password: "12345"
      });
      const {accessToken} = adminLoginResponse.data;
      // Login user
      const loginResponse = await axios.post(prepare("/login"), {
        email: "succesfuluser@test.com",
        password: "12345"
      });
    
      const userToDelete = loginResponse.data.user.id; 
      const deleteResponse = await axios.delete(prepare(`/user/${userToDelete}`), {
        headers: { Authorization: `Bearer ${accessToken}` }
      });
    
      expect(deleteResponse.status).toEqual(200);
    });

    it("should prevent non-admin users from deleting users", async () => {
      const nonAdminLoginResponse = await axios.post(prepare("/login"), {
        email: "testuser@test.com",
        password: "12345"
      });
      const {accessToken} = nonAdminLoginResponse.data;
    
      const userToDelete = nonAdminLoginResponse.data.user.id;
      await axios.delete(prepare(`/user/${userToDelete}`), {
        headers: { Authorization: `Bearer ${accessToken}` }
      }).catch((error) => {
        const {response} = error;
        expect(response.status).toEqual(403);
        expect(response.data.message).toEqual("Unauthorized Access.");
      });
    });

    it("should prevent admins from deleting other admins", async () => {
      const adminLoginResponse = await axios.post(prepare("/login"), {
        email: "test@test.com",
        password: "12345"
      });
      const {accessToken} = adminLoginResponse.data;
      // login admin 2
      const adminLoginResponse2 = await axios.post(prepare("/login"), {
        email: "test2@test.com",
        password: "12345"
      });
    
      const adminToDelete = adminLoginResponse2.data.user.id; // Replace with another admin's user ID
      await axios.delete(prepare(`/user/${adminToDelete}`), {
        headers: { Authorization: `Bearer ${accessToken}` }
      }).catch((error) => {
        const {response} = error;
        expect(response.status).toEqual(403);
        expect(response.data.message).toEqual("Unauthorized Access - Admins can not delete admins.");
      });
    });

    it("should return Bad Request for invalid user ID", async () => {
      const adminLoginResponse = await axios.post(prepare("/login"), {
        email: "test@test.com",
        password: "12345"
      });
      const {accessToken} = adminLoginResponse.data;
    
      await axios.delete(prepare(`/user/invalidUserId`), {
        headers: { Authorization: `Bearer ${accessToken}` }
      }).catch((error) => {
        const {response} = error;
        expect(response.status).toEqual(400);
        expect(response.data.message).toEqual("Bad Request.");
      });
    });
});

describe("OrderBot API Tests - Orders", () => {
  let adminLogin = null;
  let simpleConfig = null;

  beforeAll(async () => {

    // Login all related users.

    adminLogin = await axios.post(prepare("/login/"), {
      email: "test@test.com",
      password: "12345"
    });

    const login = await axios.post(prepare("/login/"), {
      email: "testuser@test.com",
      password: "12345"
    });

    const {accessToken} = login.data;

    simpleConfig = {
        headers: { Authorization: `Bearer ${accessToken}` }
    };

    const login2 = await axios.post(prepare("/login/"), {
      email: "testusertodelete@test.com",
      password: "12345"
    });
    const {accessToken2} = login2.data;

    simpleConfig2 = {
        headers: { Authorization: `Bearer ${accessToken2}` }
    };
  });

  // Order Creation Tests
  it("should create an order with valid details", async () => {
    // Insert order.
    const insertedResponse = await axios.post(prepare("/order"), {
      "type": "Box2"
    }, simpleConfig);
    
    // Get previously inserted object and check.
    const orderResponse = await axios.get(prepare("/order/") + insertedResponse.data._id, simpleConfig);
    const {data} = orderResponse;

    expect(data.type).toEqual("Box2");
    expect(orderResponse.status).toEqual(200);
  });

  it("should fail to create an order with missing fields", async () => {
    try {
      // Attempt to insert order with missing fields.
      await axios.post(prepare("/order"), {"type": ""}, simpleConfig);
      // If the request does not throw an error, fail the test.
      fail("Order creation should have failed with missing fields.");
    } catch (error) {
      // Check if the error status code is as expected
      expect(error.response.status).toEqual(400);
      // Optionally, you can assert the error message or response body.
    }
  });

  it("should fail to create an order with invalid data", async () => {
    try {
      // Attempt to insert order with invalid data.
      await axios.post(prepare("/order"), { "type": "Box3" }, simpleConfig);
      // If the request does not throw an error, fail the test.
      fail("Order creation should have failed with invalid data.");
    } catch (error) {
      // Check if the error status code is as expected 
      expect(error.response.status).toEqual(400);
      // Optionally, you can assert the error message or response body.
    }
  });

  // Order Update Tests
  it("should update an order with valid changes", async () => {
    // Insert order.
    const insertedResponse = await axios.post(prepare("/order"), {
      "type": "Box2"
    }, simpleConfig);

    // Update order.
    const updated = await axios.put(prepare("/order/"), {
      "_id": insertedResponse.data._id,
      "type": "Box1",
      "description": "{Test Order Updated}"
    }, simpleConfig);
    

    // Get previously inserted object and check.
    const orderResponse = await axios.get(prepare("/order/") + insertedResponse.data._id, simpleConfig);
    const {data} = orderResponse;
    
    expect(orderResponse.status).toEqual(200);
    expect(data.type).toEqual("Box1");
    expect(data.description).toEqual("{Test Order Updated}");
  });

  it("should fail to update a non-existent order", async () => {
    try {
      await axios.get(prepare("/order/") + "invalidID", simpleConfig);
      
      // If the request does not throw an error, fail the test
      fail("Updating a non-existent order should have failed.");
    } catch (error) {
      expect(error.response.status).toEqual(404); // Expect a 404 status code
    }
  });
  

  it("should fail to update an order with invalid data", async () => {
    try {
      // Insert order.
      const insertedResponse = await axios.post(prepare("/order"), {
        "type": "Box2"
      }, simpleConfig);

      // Update order with wrong info.
      const updated = await axios.put(prepare("/order/"), {
        "_id": insertedResponse.data._id,
        "type": "Box3",
        "description": "{Test Order Updated}"
      }, simpleConfig);

      // If the request does not throw an error, fail the test.
      fail("Updating a non-existent order should have failed.");
    } catch (error) {
      expect(error.response.status).toEqual(400);
    }
  });

  it("should fail to update an order due to unauthorized access", async () => {
    try {
      // Attempt to update an order that doesn't belong to the non-admin user
      const insertedResponse = await axios.post(prepare("/order"), {
        "type": "Box2"
      }, simpleConfig2);

      // Update order with wrong id.
      const updated = await axios.put(prepare("/order/"), {
        "_id": insertedResponse.data._id,
        "type": "Box1",
        "description": "{Test Order Updated}"
      }, simpleConfig);

      // If the request does not throw an error, fail the test
      fail("Updating an order as a non-admin user should have failed due to unauthorized access.");
    } catch (error) {
      expect(error.response.status).toEqual(403); // Expect a 403 status code for unauthorized access
      expect(error.response.data.message).toEqual("Unauthorized access.");
    }
  });
  

  // Order Deletion Tests
  it("should delete an order with a valid ID", async () => {
    const response = await axios.post(prepare("/order"), {
      "type": "Box2",
      "description": "{Test Order}"
    }, simpleConfig);

    const {data} = response;
    
    await axios.delete(prepare("/order/" + data._id), simpleConfig);
    await axios.get(prepare("/order/" + data._id), simpleConfig).catch(error => {
      expect(error.response.status).toEqual(404);
    });
    expect(response.status).toEqual(201);
  });

  it("should fail to delete a non-existent order", async () => {
    try {
      // Attempt to delete an order with an ID that doesn't exist.
      await axios.delete(prepare("/order/nonexistentOrderId"), simpleConfig);
      // If the request does not throw an error, fail the test.
      fail("Deleting a non-existent order should have failed.");
    } catch (error) {
      expect(error.response.status).toEqual(400);
    }
  });

  it("should fail to delete an order with an invalid ID", async () => {
    try {
      const insertedOrderResponse = await axios.post(prepare("/order"), {
        "type": "Box2",
        "description": "{Test Order}"
      }, simpleConfig);
      // Attempt to delete an order with an invalid ID format.
      await axios.delete(prepare("/order/invalidOrderId"), simpleConfig);
      // If the request does not throw an error, fail the test.
      fail("Deleting an order with an invalid ID should have failed.");
    } catch (error) {
      // Check if the error status code is 400 (Bad Request) or another appropriate error code.
      expect(error.response.status).toEqual(400);
    }
  });

  it("should fail to delete an order due to unauthorized access", async () => {
    try {
      // Attempt to update an order that doesn't belong to the non-admin user
      const response = await axios.post(prepare("/order"), {
        "type": "Box2"
      }, simpleConfig2);

      const {data} = response;
  
      // Attempt to delete an order that doesn't belong to the non-admin user
      await axios.delete(prepare("/order/" + data._id), simpleConfig);
  
      // If the request does not throw an error, fail the test
      fail("Deleting an order as a non-admin user should have failed due to unauthorized access.");
    } catch (error) {
      expect(error.response.status).toEqual(403); // Expect a 403 status code for unauthorized access
      expect(error.response.data.message).toEqual("Unauthorized access.");
    }
  });
  

  // Order Retrieval Tests
  it("should retrieve order details with a valid ID", async () => {
    // First, create a valid order.
    const insertedOrderResponse = await axios.post(prepare("/order"), {
      "type": "Box2",
      "description": "{Test Order}"
    }, simpleConfig);

    const { data: insertedOrderData } = insertedOrderResponse;

    // Retrieve the order using its ID.
    const retrievedOrderResponse = await axios.get(prepare("/order/" + insertedOrderData._id), simpleConfig);
    const { data: retrievedOrderData } = retrievedOrderResponse;

    // Assert that the retrieved order details match what was inserted.
    expect(retrievedOrderData._id).toEqual(insertedOrderData._id);
    expect(retrievedOrderData.type).toEqual("Box2");
    expect(retrievedOrderData.description).toEqual("{Test Order}");
    expect(retrievedOrderResponse.status).toEqual(200);
  });

  it("should retrieve order details with an invalid userID", async () => {
    try {
      // First, create a valid order.
      const insertedOrderResponse = await axios.post(prepare("/order"), {
        "type": "Box2",
        "description": "{Test Order}"
      }, simpleConfig2);

      const { data: insertedOrderData } = insertedOrderResponse;
      // Retrieve the order using its ID.
      const response = await axios.get(prepare("/order/" + insertedOrderData._id), simpleConfig);

      fail("Retrieving an order with an invalid userID should have failed.");
    } catch (error) {
      expect(error.response.status).toEqual(403);
    }
  });

  it("should fail to retrieve details of a non-existent order", async () => {
    try {
      // Attempt to retrieve an order with an ID that doesn't exist.
      await axios.get(prepare("/order/nonexistentOrderId"), simpleConfig);
      // If the request does not throw an error, fail the test.
      fail("Retrieving a non-existent order should have failed.");
    } catch (error) {
      expect(error.response.status).toEqual(400);
    }
  });

  it("should fail to retrieve order details with an invalid ID", async () => {
    try {
      // Attempt to retrieve an order with an invalid ID format.
      await axios.get(prepare("/order/invalidOrderId"), simpleConfig);
      // If the request does not throw an error, fail the test.
      fail("Retrieving order details with an invalid ID should have failed.");
    } catch (error) {
      // Check if the error status code is 400 (Bad Request) or another appropriate error code.
      expect(error.response.status).toEqual(400);
    }
  });

  it("should retrieve all order details with a valid token", async () => {
    // Assuming 'simpleConfig' contains the valid token in its headers.
    try {
      // Retrieve all orders for the logged-in user.
      const response = await axios.get(prepare("/orders/all"), simpleConfig);
  
      // Check if the response status is 200 OK.
      expect(response.status).toEqual(200);
  
      // Optionally, check the response data structure.
      // e.g., expect(response.data).toBeInstanceOf(Array);
    } catch (error) {
      // Fail the test if an error occurs.
      fail("Retrieving all orders with a valid token should not have failed.");
    }
  });

  it("should fail to retrieve all order details with an invalid token", async () => {
    await axios.get(prepare("/orders/user/" + adminLogin.data.user.id), simpleConfig).catch(error => {
      // Unauthorized
      expect(error.response.status).toEqual(403);
    });
  });
});



/*
const axios = require("axios");
const {prepare} = require("../setup/test-helper");

describe("Generic User Tests", () => {

  let adminLogin = null;
  let genericConfig = null;

  beforeAll(async () => {
    // Login admin user.

    adminLogin = await axios.post(prepare("/login"), {
      email: "test@test.com",
      password: "12345"
    });

    const {accessToken} = adminLogin.data;

    // Note we use JWT authentication for the API,
    // therefore we need to authenticate our request for the test.
    genericConfig = {
      headers: { Authorization: `Bearer ${accessToken}` }
    } 
  });

  it("should check system is on", async () => {
    const response = await axios.get(prepare("/"));
    expect(response.status).toEqual(200);
  });


  it("should login user", async () => {
    const userLogin = await axios.post(prepare("/login"), {
      email: "testuser@test.com",
      password: "12345"
    });

    const {accessToken} = userLogin.data;
    expect(userLogin.status).toEqual(200);
    expect(accessToken).not.toEqual(undefined);
  });


  it("should fail to login user (wrong password)", async () => {
    await axios.post(prepare("/login"), {
      email: "testuser@test.com",
      password: "1234567"
    }).catch((error) => {
      const {response} = error;
      const {accessToken} = response.data;
      expect(response.status).toEqual(401);
      expect(accessToken).toEqual(null);
    });
    
  });

  it("should hit random endpoint", async () => {
    await axios.get(prepare("/randomurl/something")).catch(error => {
      // 401 - Unauthorized Access
      expect(error.response.status).toEqual(404);
    });
  });

  it("should fail unauthorized access actions", async () => {

    await axios.get(prepare("/users")).catch(error => {
      // 401 - Unauthorized Access
      expect(error.response.status).toEqual(401);
    });
  
    await axios.get(prepare("/orders/all")).catch(error => {
      // 401 - Unauthorized Access
      expect(error.response.status).toEqual(401);
    });
  
    await axios.get(prepare("/orders/user/" + adminLogin.data.user.id)).catch(error => {
      // 401 - Unauthorized Access
      expect(error.response.status).toEqual(401);
    });

    await axios.post(prepare("/order"), {
      "type": "Box2",
      "description": "{Test Order}"
    }).catch(error => {
      // 401 - Unauthorized Access
      expect(error.response.status).toEqual(401);
    });

    await axios.put(prepare("/order"), {
      "type": "Box1",
      "description": "{Test Order}"
    }).catch(error => {
      // 401 - Unauthorized Access
      expect(error.response.status).toEqual(401);
    });

    await axios.delete(prepare("/order/12345"), {
      "type": "Box2",
      "description": "{Test Order}"
    }).catch(error => {
      // 401 - Unauthorized Access
      expect(error.response.status).toEqual(401);
    });

    await axios.put(prepare("/me"), {
      "name": "ShouldNotUpdate"
    }).catch(error => {
      // 401 - Unauthorized Access
      expect(error.response.status).toEqual(401);
    });

    await axios.delete(prepare("/user/12345"), {
      "type": "Box2",
      "description": "{Test Order}"
    }).catch(error => {
      // 401 - Unauthorized Access
      expect(error.response.status).toEqual(401);
    });
    
  });

  it("should register user", async () => {
    const response = await axios.post(prepare("/register"), {
      "name": "User New",
      "role": "User",
      "email": "testusernew@test.com",
      "password": "12345",
      "address": "Somewhere 10"
    });
    expect(response.status).toEqual(201);
  });

  it("should fail to register user (existing email)", async () => {

    
    const response = await axios.post(prepare("/register"), {
      "name": "User New",
      "role": "User",
      "email": "testuserbrandnew@test.com",
      "password": "12345",
      "address": "Somewhere 10"
    });

    expect(response.status).toEqual(201);

    await axios.post(prepare("/register"), {
      "name": "User New",
      "role": "User",
      "email": "testuserbrandnew@test.com",
      "password": "12345",
      "address": "Somewhere 10"
    }).catch(error => {
      // Conflict - user exists.
      expect(error.response.status).toEqual(409);
    });

    
  });

  it("should fail to register user (malformed email)", async () => {
    await axios.post(prepare("/register"), {
      "name": "User New",
      "role": "User",
      "email": "testuseranothernew",
      "password": "12345",
      "address": "Somewhere 10"
    }).catch(error => {
      expect(error.response.status).toEqual(400);
    });
  });

  it("should fail to register user (no password)", async () => {
    await axios.post(prepare("/register"), {
      "name": "User New",
      "role": "User",
      "email": "testuseranothernew2@test.com",
      "address": "Somewhere 10"
    }).catch(error => {
      expect(error.response.status).toEqual(400);
    });
  });

  it("should fail to register user (no name)", async () => {
    await axios.post(prepare("/register"), {
      "role": "User",
      "email": "testuseranothernew3@test.com",
      "password": "12345",
      "address": "Somewhere 10"
    }).catch(error => {
      expect(error.response.status).toEqual(400);
    });
  });

  it("should fail to register user (no address)", async () => {
    await axios.post(prepare("/register"), {
      "name": "User New",
      "role": "User",
      "email": "testuseranothernew4@test.com",
      "password": "12345",
    }).catch(error => {
      expect(error.response.status).toEqual(400);
    });
  });

  it("should block access by valid auth token of non-existing user", async () => {
    await axios.post(prepare("/register"), {
      "name": "UserToDeleteNow",
      "role": "User",
      "email": "testtodeletenow@test.com",
      "password": "12345",
      "address": "Somewhere 10"
    });

    userToDeleteNow = await axios.post(prepare("/login/"), {
      email: "testtodeletenow@test.com",
      password: "12345"
    });

    const {accessToken} = userToDeleteNow.data
    userToDeletegenericConfig = {
      headers: { Authorization: `Bearer ${accessToken}` }
    }

    await axios.delete(prepare("/user/" + userToDeleteNow.data.user.id), genericConfig);

    // Attempt to get orders of deleted user, while access token still valid.
    await axios.get(prepare("/orders/all"), userToDeletegenericConfig).catch(error => {
      // Unauthorized.
      expect(error.response.status).toEqual(403);
    });
  
  });

});
*/